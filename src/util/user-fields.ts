import { DateTime } from 'luxon'
import { Field } from './field'

export const fields: (Field & { editable: boolean; hideable: boolean })[] = [
  {
    id: 'email',
    name: 'Email',
    icon: 'mdi-email',
    editable: false,
    hideable: true,
    rules: 'email|required'
  },
  {
    id: 'fullname',
    name: 'Full Name',
    icon: 'mdi-account',
    editable: false,
    hideable: false,
    rules: 'required'
  },
  {
    id: 'birthDate',
    name: 'Date of Birth',
    icon: 'mdi-cake',
    type: 'date',
    editable: false,
    hideable: true,
    display: (v: string) => v && DateTime.fromISO(v).toFormat('dd/MM/yyyy'),
    rules: 'required'
  },
  {
    id: 'phone',
    name: 'Phone Number',
    icon: 'mdi-phone',
    editable: true,
    hideable: true,
    rules: 'required',
    mask: (v) => v.length < 15 ? '(##) ####-####' : '(##) #####-####'
  },
  {
    id: 'RG',
    name: 'RG',
    icon: 'mdi-card-account-details',
    editable: false,
    hideable: false,
    rules: 'required'
  },
  {
    id: 'CPF',
    name: 'CPF',
    icon: 'mdi-card-account-details',
    editable: false,
    hideable: false,
    rules: 'required',
    mask: '###.###.###-##'
  }
]
