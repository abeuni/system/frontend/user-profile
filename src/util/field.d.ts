export interface BasicField {
  id: string;
  name: string;
  icon: string;
  display?: (v: string) => string;
  rules?: string;
}

export interface DateField {
  type: 'date';
  min?: string;
  max?: string;
}

export interface EnumField {
  type: 'enum';
  valField?: string;
  textField?: string;
  options: ((field: unknown) => Promise<unknown[]>) | { value: string; name: string }[];
}

export interface TextField {
  type?: undefined | 'text';
  mask?: string | ((v: string) => string);
}

export interface TextArea {
  type: 'textarea';
}

export type Field = BasicField & (DateField | EnumField | TextField | TextArea)
