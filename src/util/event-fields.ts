import { Field } from './field'

export const fields: Field[] = [
  {
    id: 'category',
    name: 'Category',
    icon: 'mdi-shape',
    type: 'enum',
    options: [
      {
        value: 'CARAVAN',
        name: 'Caravan'
      }
    ],
    rules: 'required'
  }
]
