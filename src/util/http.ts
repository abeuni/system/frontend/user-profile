import axios, { AxiosRequestConfig } from 'axios'
import { app } from '@/main'

const basePath = process.env.VUE_APP_BASE_PATH || 'https://api.system.abeuni.org.br/'

const makeURL = (path: string): string => {
  if (path.startsWith('http')) {
    return path
  }

  return basePath + path
}

const makeOptions = async (options?: AxiosRequestConfig): Promise<AxiosRequestConfig> => {
  const config: AxiosRequestConfig = {
    ...options
  }

  if (!config.headers) config.headers = {}

  config.headers.authorization = `Bearer ${await app.$auth.getTokenSilently()}`

  return config
}

export const get = async (path: string, options?: AxiosRequestConfig) => {
  return axios.get(makeURL(path), await makeOptions(options))
}

export const post = async (path: string, data?: unknown, options?: AxiosRequestConfig) => {
  return axios.post(makeURL(path), data, await makeOptions(options))
}
