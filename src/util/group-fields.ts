import { Field } from './field'

export enum GroupCategory {
  DEPARTMENT = 'DEPARTMENT',
  COMISSION = 'COMISSION',
  COMMITTEE = 'COMMITTEE',
  WORKGROUP = 'WORKGROUP'
}

export const fields: Field[] = [
  {
    id: 'category',
    name: 'Category',
    icon: 'mdi-shape',
    type: 'enum',
    options: [
      {
        value: GroupCategory.DEPARTMENT,
        name: 'Department'
      },
      {
        value: GroupCategory.COMISSION,
        name: 'Comission'
      },
      {
        value: GroupCategory.COMMITTEE,
        name: 'Committee'
      },
      {
        value: GroupCategory.WORKGROUP,
        name: 'Workgroup'
      }
    ],
    rules: 'required'
  },
  {
    id: 'name',
    name: 'Name',
    icon: 'mdi-account-group',
    rules: 'required|min:5'
  },
  {
    id: 'code',
    name: 'Code',
    icon: 'mdi-code-brackets',
    mask: 'AAAAAAAA',
    rules: 'min:3|max:8'
  },
  {
    id: 'description',
    name: 'Description',
    icon: 'mdi-text-box',
    type: 'textarea'
  }
]
