import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'root',
    redirect: { name: 'redirector' }
  },
  {
    path: '/redirector',
    name: 'redirector',
    component: () => import('@/views/Redirector.vue')
  },
  {
    path: '/auth-callback',
    name: 'auth-callback',
    component: () => import('@/views/Callback.vue')
  },
  {
    path: '/profile',
    name: 'profile',
    component: () => import('@/views/Profile.vue')
  },
  {
    path: '/profile/:id',
    name: 'user-profile',
    component: () => import('@/views/Profile.vue')
  },
  {
    path: '/create-user',
    name: 'create-user',
    component: () => import('@/views/CreateUser.vue')
  },
  {
    path: '/create-group',
    name: 'create-group',
    component: () => import('@/views/CreateGroup.vue')
  },
  {
    path: '/create-event',
    name: 'create-event',
    component: () => import('@/views/CreateEvent.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
