import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import '@/plugins/vuemask'
import '@/plugins/vee-validate'
import { Auth0Plugin } from './plugins/auth0'

Vue.use(Auth0Plugin, {
  domain: process.env.VUE_APP_AUTH0_DOMAIN,
  clientId: process.env.VUE_APP_AUTH0_CLIENT,
  audience: 'main-system',
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  onRedirectCallback: (appState: any) => {
    if (appState && appState.targetUrl) {
      router.push(appState.targetUrl)
    }
  }
})

Vue.config.productionTip = false

export const app = new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
