import Vue from 'vue'
import VueMask from 'v-mask'

Vue.use(VueMask, {
  placeholders: {
    Z: /[A-Z]/ // define new placeholder
  }
})
